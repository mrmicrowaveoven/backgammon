import React from 'react';
import Space from './Space.js';

class Board extends React.Component {
  constructor(props) {
    super(props);
  }

  corner(directions) {
    const cornerClassname = 'corner ' + directions.join('-')
    return (
      <div className={cornerClassname}>
        <Space corner={directions.join(' ')} />
        <Space corner={directions.join(' ')} />
        <Space corner={directions.join(' ')} />
        <Space corner={directions.join(' ')} />
        <Space corner={directions.join(' ')} />
        <Space corner={directions.join(' ')} />
      </div>
    )
  }

  render() {
    return (
      <div className="board">
        {this.corner(['top', 'right'])}
        {this.corner(['top', 'left'])}
        {this.corner(['bottom', 'left'])}
        {this.corner(['bottom', 'right'])}
      </div>
    );
  }
}

export default Board;
