import React from 'react';

class Space extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {corner} = this.props;
    const nameOfClass = 'space ' + corner
    return (
      <div>
        <div className={nameOfClass} />
      </div>
    );
  }
}

export default Space;
